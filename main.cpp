/*
 * Copyright (c) 2023 TranslucentFoxHuman (aka. TlFoxHuman, 半透明狐人間, or 半狐)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
       if (argc >= 2){
           bool helpflag =false;
           bool licenseflag =false;
           int invalidcount = 0;
           for (int i = 1;argc > i;i++) {
               if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help") {
                  helpflag = true;
               }else if (std::string(argv[i]) == "--license") {
                   licenseflag = true;
               } else {
                   invalidcount = i;
               }
           }
           if (invalidcount != 0){
               std::cout << "Error: Invalid option: " << argv[invalidcount] << std::endl;
               return 0;
           }else if (helpflag == true){
               std::cout << "Usage: " << argv[0] << " [Options]\n\n"
                            "SimpleClock - A very simple clock like as \"xclock -digital\".\n\n"
                            "Options:\n"
                            "-h, --help    Show this help.\n"
                            "--license     Show license message."<< std::endl;
               return 0;
           } else if (licenseflag == true) {
               std::cout << "Permission is hereby granted, free of charge, to any person obtaining a\n"
                            "copy of this software and associated documentation files (the \"Software\"),\n"
                            "to deal in the Software without restriction, including without limitation\n"
                            "the rights to use, copy, modify, merge, publish, distribute, sublicense,\n"
                            "and/or sell copies of the Software, and to permit persons to whom the\n"
                            "Software is furnished to do so, subject to the following conditions:\n"
                            "\n"
                            "The above copyright notice and this permission notice shall be included in\n"
                            "all copies or substantial portions of the Software.\n"
                            "\n"
                            "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n"
                            "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n"
                            "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n"
                            "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n"
                            "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING\n"
                            "FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER\n"
                            "DEALINGS IN THE SOFTWARE." << std::endl;
               return 0;
           }
       }
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
